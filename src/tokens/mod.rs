// Main Token - Recursively matched on by the Parser to build the AST
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub(crate) enum Token<'a> {
    // Produced by running the source through the lexer. The leaves of the AST
    Raw(LexerToken<'a>),
    // Selector(Selectors),
    // Media,
    // Comment,
    // Animation,
    // CSS,
}

// LexerComponents operate on raw character streams and produce two types of
// tokens. These are wrapped in Token::Raw so the Parser can understand them.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub(crate) enum LexerToken<'a> {
    // The character matched a ReservedCharacter
    Char(ReservedChar),
    // Reference into the source str slice for everything else
    Str(&'a str),
}

// Re-export the reserved characters enum
mod reserved_characters;
pub(crate) use reserved_characters::ReservedChar;

use std::convert::TryFrom;

// ReservedCharacters have (potentially) special meaning in CSS. This file
// lists the reserved characters, and implements handy functions for
// matching, printing and tokenizing them.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub(crate) enum ReservedChar {
    Comma,
    Period,
    GreaterThan,
    OpenParenthese,
    CloseParenthese,
    OpenCurlyBrace,
    CloseCurlyBrace,
    OpenBracket,
    CloseBracket,
    Colon,
    SemiColon,
    Slash,
    Plus,
    EqualSign,
    Space,
    Tab,
    Newline,
    CarriageReturn,
    Star,
    Quote,
    DoubleQuote,
    Pipe,
    Tilde,
    Dollar,
    Circumflex,
}

// Useful for debugging, needs a simillar implementation that doesn't use
// string formatting for the final version (maybe).
impl std::fmt::Display for ReservedChar {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match *self {
                ReservedChar::Comma => ',',
                ReservedChar::Period => '.',
                ReservedChar::OpenParenthese => '(',
                ReservedChar::CloseParenthese => ')',
                ReservedChar::OpenCurlyBrace => '{',
                ReservedChar::CloseCurlyBrace => '}',
                ReservedChar::OpenBracket => '[',
                ReservedChar::CloseBracket => ']',
                ReservedChar::Colon => ':',
                ReservedChar::SemiColon => ';',
                ReservedChar::Slash => '/',
                ReservedChar::Star => '*',
                ReservedChar::Plus => '+',
                ReservedChar::EqualSign => '=',
                ReservedChar::Space => ' ',
                ReservedChar::Tab => '\t',
                ReservedChar::Newline => '\n',
                ReservedChar::CarriageReturn => '\r',
                ReservedChar::GreaterThan => '>',
                ReservedChar::Quote => '\'',
                ReservedChar::DoubleQuote => '"',
                ReservedChar::Pipe => '|',
                ReservedChar::Tilde => '~',
                ReservedChar::Dollar => '$',
                ReservedChar::Circumflex => '^',
            }
        )
    }
}

// Matches a character to the enum variant. Returns None if it fails to match.
impl TryFrom<char> for ReservedChar {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            ',' => Ok(ReservedChar::Comma),
            '.' => Ok(ReservedChar::Period),
            '(' => Ok(ReservedChar::OpenParenthese),
            ')' => Ok(ReservedChar::CloseParenthese),
            '{' => Ok(ReservedChar::OpenCurlyBrace),
            '}' => Ok(ReservedChar::CloseCurlyBrace),
            '[' => Ok(ReservedChar::OpenBracket),
            ']' => Ok(ReservedChar::CloseBracket),
            ':' => Ok(ReservedChar::Colon),
            ';' => Ok(ReservedChar::SemiColon),
            '/' => Ok(ReservedChar::Slash),
            '*' => Ok(ReservedChar::Star),
            '+' => Ok(ReservedChar::Plus),
            '=' => Ok(ReservedChar::EqualSign),
            ' ' => Ok(ReservedChar::Space),
            '\t' => Ok(ReservedChar::Tab),
            '\n' => Ok(ReservedChar::Newline),
            '\r' => Ok(ReservedChar::CarriageReturn),
            '\'' => Ok(ReservedChar::Quote),
            '\"' => Ok(ReservedChar::DoubleQuote),
            '>' => Ok(ReservedChar::GreaterThan),
            '|' => Ok(ReservedChar::Pipe),
            '~' => Ok(ReservedChar::Tilde),
            '$' => Ok(ReservedChar::Dollar),
            '^' => Ok(ReservedChar::Circumflex),
            _ => Err(()),
        }
    }
}

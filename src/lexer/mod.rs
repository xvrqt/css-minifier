// Import the LexerComponents
mod component;
use component::{RawStrSliceMatcher, ReservedCharacterMatcher};

// Import the token types.
// Lexer Components operate on chars which are turned into LexerTokens.
// The Lexer wraps these in the main Token type before returning.
use crate::tokens::{LexerToken, Token};

// When input is passed to a LexerComponent, it returns whether or not it was
// able to match to the pattern and return a LexerToken
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub(crate) enum Match<'a> {
    // Components start in the Unknown state when they are reset
    Unknown,
    // When a component cannot possibly match, even with more input. Prevents
    // component from running it's check when it's impossible.
    Failed,
    // If the component could match on additional input, but hasn't confirmed
    // a match yet.
    Possible,
    // The component has matched, here is the token it would return. Note that
    // the token may not beused because other components with higher prioirty
    // may have matched (or still be trying to match).
    Success(LexerToken<'a>),
}

impl<'a> Match<'a> {
    // Acts like Option .or() but with Match::Failed as the None case
    pub fn or(self, match_b: Match<'a>) -> Match<'a> {
        match self {
            Match::Success(_) => self,
            Match::Possible => self,
            // Unknown is used as the initial accumulator value when we fold
            // the Match's and so should be treated as None
            //
            // Failed is the equivalent of None
            _ => match_b,
        }
    }
}

// Lexer components operate on a str slice representing the source code and
// return whether or not they matched, and if they did match, the LexerToken
// representing the match.
//
// Since str slices are UTF-8 the indices are needed to calculate the substring
// slice ranges.
pub(crate) trait LexerComponent<'a> {
    // .check()
    // TAKES:
    //   - Tuple of the current character's index and the character itself
    //   - Optional Tuple of the next character's index and the character itself.
    //       - Is NONE when there is no next character
    //   - str slice representing the source code
    // RETURNS:
    //   - Optional enum of type Match indicating if the lexer component matches
    //     the character stream inputted.
    fn check(
        &mut self,
        current: (usize, char),
        next: Option<&(usize, char)>,
        source: &'a str,
    ) -> Match<'a>;

    // .reset()
    // BEHAVIOR:
    //   - Resets the component, erasing any progress towards the next match. This
    //     is called when a match is found in any component.
    fn reset(&mut self);
}

// The lexer is construced of a list of Lexer Components in order of their
// match priority. The lexer takes in a str slice representing the source code
//  and pass it as a stream of characters to each LexerComponent, until one of
// them returns a successful match. It compiles these LexerTokens into a Vec of
// Tokens that the parser can understand and operate on.
pub(crate) struct Lexer<'a> {
    components: Vec<Box<dyn LexerComponent<'a> + 'a>>,
}

impl<'a> Lexer<'a> {
    pub(crate) fn new() -> Lexer<'a> {
        Lexer {
            // The order of the components is vital!
            components: vec![
                Box::new(ReservedCharacterMatcher::new()),
                Box::new(RawStrSliceMatcher::new()),
            ],
        }
    }

    // Calls .check() on each component in priority order and returns the
    // match status of the first successful or possibly successful match.
    fn check_all_components(
        &mut self,
        current: (usize, char),
        next: Option<&(usize, char)>,
        source: &'a str,
    ) -> Match<'a> {
        self.components.iter_mut().fold(Match::Unknown, |acc, c| {
            acc.or(c.check(current, next, source))
        })
    }

    // Calls .reset() on each lexer component
    fn reset_all_components(&mut self) {
        self.components.iter_mut().for_each(|c| c.reset());
    }

    // Transforms the original string into a series of reserved characters and names
    // The functions in this first pass are never used again so this saves later
    // passes from having to check them all. i.e. it transforms the text into a
    // token stream that can be recursively matched upon.
    pub(crate) fn lex(&mut self, source: &'a str) -> Result<Vec<Token<'a>>, &'static str> {
        let mut v = Vec::with_capacity(source.len() / 2);

        let mut chars = source.char_indices().peekable();
        while let Some(current) = chars.next() {
            let next = chars.peek();

            // Push on success
            let result = self.check_all_components(current, next, source);
            if let Match::Success(token) = result {
                v.push(Token::Raw(token));
                self.reset_all_components();
            }
        }
        Ok(v)
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::tokens::ReservedChar;

    #[test]
    fn trivial() {
        let _lexer = Lexer::new();
        assert!(true);
    }

    #[test]
    fn lex_test_rc_only() {
        let mut l = Lexer::new();
        let t = Token::Raw(LexerToken::Char(ReservedChar::Comma));

        let actual = l.lex(",,,").unwrap();
        let expected = vec![t, t, t];

        assert_eq!(actual, expected);
    }

    #[test]
    fn lex_test_str_only() {
        let mut l = Lexer::new();

        let actual = l.lex("lesbian").unwrap();
        let expected = vec![Token::Raw(LexerToken::Str("lesbian"))];

        assert_eq!(actual, expected);
    }

    #[test]
    fn lex_test() {
        let mut l = Lexer::new();
        let expected = vec![
            Token::Raw(LexerToken::Str("gay")),
            Token::Raw(LexerToken::Char(ReservedChar::Space)),
            Token::Raw(LexerToken::Str("girls")),
        ];

        let actual = l.lex("gay girls").unwrap();

        assert_eq!(actual, expected);
    }

    #[test]
    fn css_test() {
        let mut l = Lexer::new();

        let actual = l.lex(".gay { height: 10px }").unwrap();
        let expected = vec![
            Token::Raw(LexerToken::Char(ReservedChar::Period)),
            Token::Raw(LexerToken::Str("gay")),
            Token::Raw(LexerToken::Char(ReservedChar::Space)),
            Token::Raw(LexerToken::Char(ReservedChar::OpenCurlyBrace)),
            Token::Raw(LexerToken::Char(ReservedChar::Space)),
            Token::Raw(LexerToken::Str("height")),
            Token::Raw(LexerToken::Char(ReservedChar::Colon)),
            Token::Raw(LexerToken::Char(ReservedChar::Space)),
            Token::Raw(LexerToken::Str("10px")),
            Token::Raw(LexerToken::Char(ReservedChar::Space)),
            Token::Raw(LexerToken::Char(ReservedChar::CloseCurlyBrace)),
        ];

        assert_eq!(actual, expected);
    }
}

// This lexer component matches every Reserved Character.
// See reserved_charaters.rs for a list of Reserved Characters.
//
// Returns a Token::Raw(Lexical::Char(ReservedChar))

use crate::lexer::{LexerComponent, Match};
use crate::tokens::{LexerToken, ReservedChar};
use std::convert::TryFrom;

#[derive(Debug, PartialEq, Eq, Clone)]
pub(crate) struct ReservedCharacterMatcher<'a> {
    status: Match<'a>,
}

// New function to keep things smöl and DRY
impl<'a> ReservedCharacterMatcher<'_> {
    pub(crate) fn new() -> Self {
        ReservedCharacterMatcher {
            status: Match::Unknown,
        }
    }
}

impl<'a> LexerComponent<'a> for ReservedCharacterMatcher<'a> {
    fn check(
        &mut self,
        current: (usize, char),
        _next: Option<&(usize, char)>,
        _source: &'a str,
    ) -> Match<'a> {
        let (_, c) = current;
        match self.status {
            Match::Unknown | Match::Possible => match ReservedChar::try_from(c) {
                Ok(rc) => {
                    self.status = Match::Success(LexerToken::Char(rc));
                }
                _ => self.status = Match::Failed,
            },
            // Match::Failed/Success return themselves
            _ => (),
        }
        self.status
    }

    fn reset(&mut self) {
        self.status = Match::Unknown;
    }
}

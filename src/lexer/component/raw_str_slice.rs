// This lexer component matches every substring of the source code that is not
// or does not contain a Reserved Character.
// See crate::token::reserved_charater.rs for a list of Reserved Characters.
use std::convert::TryFrom;

use crate::lexer::{LexerComponent, Match};
use crate::tokens::{LexerToken, ReservedChar};

#[derive(Debug, PartialEq, Eq, Clone)]
pub(crate) struct RawStrSliceMatcher<'a> {
    status: Match<'a>,
    // Start of the str slice
    start: usize,
}

impl<'a> RawStrSliceMatcher<'_> {
    pub(crate) fn new() -> Self {
        RawStrSliceMatcher {
            status: Match::Unknown,
            start: 0,
        }
    }
}

impl<'a> LexerComponent<'a> for RawStrSliceMatcher<'a> {
    fn check(
        &mut self,
        current: (usize, char),
        next: Option<&(usize, char)>,
        source: &'a str,
    ) -> Match<'a> {
        // Convenience function that checks if the string has ended (next char
        // is EOF or a ReservedChar) and returns the corresponding slice of
        // 'source'
        fn string_ends<'a>(
            source: &'a str,
            start: usize,
            next: Option<&(usize, char)>,
        ) -> Option<LexerToken<'a>> {
            match next {
                // If there is a next character...
                Some(&(n, p)) => {
                    // If that char is a Reserverd Character, end the string
                    match ReservedChar::try_from(p) {
                        Ok(_) => Some(LexerToken::Str(&source[start..n].trim())),
                        _ => None,
                    }
                }
                // Otherwise we're at the end of the stream and need end the string
                None => Some(LexerToken::Str(&source[start..].trim())),
            }
        }

        let (i, c) = current;
        match self.status {
            Match::Unknown => {
                // Str's can't be reserved characters
                match ReservedChar::try_from(c) {
                    Ok(_) => self.status = Match::Failed,
                    // If it's not a Reserved Character, start a Str Token
                    _ => {
                        self.start = i;
                        // If the string ends next character, succeed and return the slice
                        if let Some(slice) = string_ends(source, self.start, next) {
                            self.status = Match::Success(slice);
                        } else {
                            self.status = Match::Possible;
                        }
                    }
                }
            }
            Match::Possible => {
                // Check if the next character is reserved, ending the Str
                if let Some(slice) = string_ends(source, self.start, next) {
                    self.status = Match::Success(slice);
                }
            }
            // Match::Failed/Success return themselves
            _ => (),
        }
        self.status
    }

    fn reset(&mut self) {
        self.status = Match::Unknown;
    }
}

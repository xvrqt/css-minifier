mod lexer;
use lexer::Lexer;

mod tokens;
use tokens::Token;

pub struct CSSMinifier<'a> {
    lexer: Lexer<'a>,
    #[allow(dead_code)]
    tokens: Vec<Token<'a>>,
}

impl<'a> CSSMinifier<'a> {
    pub fn new() -> Self {
        CSSMinifier {
            lexer: Lexer::new(),
            tokens: Vec::new(),
        }
    }

    pub fn minify(&mut self, source: &'a str) -> Result<(), &'static str> {
        let source = source.trim();
        self.lexer.lex(source)?;
        unimplemented!();
    }
}

// Default implementation
impl<'a> Default for CSSMinifier<'a> {
    fn default() -> Self {
        Self::new()
    }
}
